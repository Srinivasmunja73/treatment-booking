package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableRowSorter;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import database.BookingTimeTable;
import database.PatientTableModel;
import database.Timetable;

public class VisitorMenu extends Settings {

	private static final long serialVersionUID = 8298157137710032378L;
	GUI_panel guipanel;
	GridBagLayout gridBagLayoutMainPanel;
	GridLayout gridLayoutpane4;
	public JPanel panel2;
	private ImageIcon icon1, icon2, icon3;
	public JButton book_appointment, search_doctor, logout, check;
	private JLabel new_Booking, appoitment_id, visitorname, category, doctor, datelbl;
	public JTextField new_bookingTxt, visitornametxt;
	public TableRowSorter<PatientTableModel> sorter;
	public JComboBox<String> categoryComboBox, doctorComboBox;

	public String line, name1, name2, name3, name4, selectedCell,appointment_time ;
	public JTable table;

	UtilDateModel model = new UtilDateModel();
	private FlowLayout flowLayoutButton;

	private final int columns = 2;
	private final int rows = 5;
	private final int vGap = 3;
	private final int hGap = 1;
	FileInputStream doctorfile;
	BufferedReader dr;

	BookingTimeTable tableModel;
	FileInputStream AppTimetable;
	
	Date selectedValue;

	public VisitorMenu() {
		setupGUI();
	}

	private void setupGUI() {

		gridBagLayoutMainPanel = new GridBagLayout();
		
		icon1 = new ImageIcon("images/visit1.png");
		icon2 = new ImageIcon("images/visit2.png");
		icon3 = new ImageIcon("images/visit3.png");

		book_appointment = new JButton(icon1);
		book_appointment.setBackground(Color.WHITE);
		book_appointment.setOpaque(false);
		book_appointment.setBorder(null);

		search_doctor = new JButton(icon2);
		search_doctor.setBackground(Color.WHITE);
		search_doctor.setOpaque(false);
		search_doctor.setBorder(null);

		logout = new JButton(icon3);
		logout.setBackground(Color.WHITE);
		logout.setOpaque(false);
		logout.setBorder(null);

		flowLayoutButton = new FlowLayout();
		flowLayoutButton.setAlignment(FlowLayout.CENTER);

		book_appointment.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				VisitorMenu log = new VisitorMenu();
				log.setVisible(true);
				// close the current window
				Component book_appointment = (Component) e.getSource();
				Window bk = SwingUtilities.windowForComponent(book_appointment);
				bk.setVisible(false);

			}

		});

		search_doctor.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				VisitorFindDoctor log = new VisitorFindDoctor();
				log.setVisible(true);
				// close the current window
				Component search_doctor = (Component) e.getSource();
				Window bk = SwingUtilities.windowForComponent(search_doctor);
				bk.setVisible(false);

			}

		});
		logout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				App_GUI log = new App_GUI();
				log.setVisible(true);
				// close the current window
				Component logout = (Component) e.getSource();
				Window bk = SwingUtilities.windowForComponent(logout);
				bk.setVisible(false);
			}

		});

		// call top panel for the image
		guipanel = new GUI_panel();
		guipanel.setPreferredSize(new Dimension(1190, 150));

		// create the second pannel
		panel2 = new JPanel(); // panel 2

		// panel 2 attributes
		panel2.setLayout(flowLayoutButton);
		panel2.setPreferredSize(new Dimension(1190, 50));
		panel2.setBackground(Color.BLUE);
		panel2.add(book_appointment);
		panel2.add(search_doctor);
		panel2.add(logout);

		JPanel panel3 = new JPanel();// creates a new panel
		// new_Booking, appoitment_id, category, doctor, datelbl
		new_Booking = new JLabel("<HTML><U>Book Appointment</U></HTML>");
		new_Booking.setForeground(Color.BLUE);
		new_Booking.setFont(new Font("Serif", Font.PLAIN, 20));
		// set panel properties and attributes
		panel3.setPreferredSize(new Dimension(1190, 30));
		panel3.add(new_Booking);
		panel3.setLayout(flowLayoutButton);

		// create booking panel
		JPanel booking = new JPanel();
		appoitment_id = new JLabel("Appointment ID :");
		visitorname = new JLabel("Visitor Name");
		category = new JLabel("category :");
		doctor = new JLabel("Doctor :");
		datelbl = new JLabel("Date :");

		// new_bookingTxt
		new_bookingTxt = new JTextField();
		visitornametxt = new JTextField();

		// categoryComboBox, doctorComboBox;
		categoryComboBox = new JComboBox<String>();

		doctorComboBox = new JComboBox<String>();

		// categoryComboBox, doctorComboBox;
		categoryComboBox = new JComboBox<String>();
		categoryComboBox.setEditable(false);
		categoryComboBox.addItem("Physiotherapy");
		categoryComboBox.addItem("Osteopathy");
		categoryComboBox.addItem("Rehabilitation");
		categoryComboBox.addItem("Neural Mobilisation");
		categoryComboBox.addItem("Acupuncture");
		categoryComboBox.addItem("Massage");
		categoryComboBox.addItem("Spine mobilisation");
		categoryComboBox.addItem("Joint mobilisation");
		doctorComboBox = new JComboBox<String>();

		categoryComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					doctorComboBox.removeAllItems();
					doctorfile = new FileInputStream("src/database/doctor.txt");
					dr = new BufferedReader(new InputStreamReader(doctorfile));
					String line;
					boolean doctorFound = false;
					while ((line = dr.readLine()) != null) {
						
						String[] tokens = line.split(",");
						if (tokens.length > 1)
							line = tokens[4];
						if (line.equals(categoryComboBox.getSelectedItem().toString())) {
							doctorFound = true;
							doctorComboBox.addItem(tokens[1]);
						}
					}
                    
                    if(doctorFound == false)
                    {
                    	new Options_Pane("Error!","No doctor found belonging to this category", 1);
                    }

				}

				catch (FileNotFoundException ex) {
					Logger.getLogger(PatientMenu.class.getName()).log(Level.SEVERE, null, ex);
				} catch (IOException ex) {
					Logger.getLogger(PatientMenu.class.getName()).log(Level.SEVERE, null, ex);
				}

			}
		});


		// addalender
		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");
		JDatePanelImpl datePanel = new JDatePanelImpl(model, p);
		JDatePickerImpl datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());

		// add components to booking panel
		gridLayoutpane4 = new GridLayout(rows, columns);
		booking.add(appoitment_id);
		booking.add(new_bookingTxt);
		booking.add(visitorname);
		booking.add(visitornametxt);
		booking.add(category);
		booking.add(categoryComboBox);
		booking.add(doctor);
		booking.add(doctorComboBox);
		booking.add(datelbl);
		booking.add(datePicker);

		booking.setLayout(gridLayoutpane4);
		gridLayoutpane4.setVgap(vGap);
		gridLayoutpane4.setHgap(hGap);

		// Panel 4 containing the button
		JPanel panel4 = new JPanel();// creates a new panel
		check = new JButton("Check");
		// set panel properties and attributes
		panel4.setPreferredSize(new Dimension(1190, 50));
		panel4.add(check);
		panel4.setLayout(flowLayoutButton);

		// create appointments timetable
		JPanel panel41 = new JPanel();
		table = new JTable();

		try {

			tableModel = new BookingTimeTable();
			AppTimetable = new FileInputStream("src/database/BookingTimeTable.txt");// load file for I/O)
			BufferedReader br = new BufferedReader(new InputStreamReader(AppTimetable));
			String line;
			List<Timetable> scheduleList = new ArrayList<Timetable>();
			// add data to patient details table
			while ((line = br.readLine()) != null) {
				String[] splitData = line.split("\\,");
				// DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-mm-dd");

				Timetable timetable = new Timetable();
				Date dateBegin = Date.valueOf(splitData[0]);
				timetable.setDay(dateBegin);
				timetable.setSchedule1(splitData[1]);
				timetable.setSchedule2(splitData[2]);
				timetable.setSchedule3(splitData[3]);
				timetable.setSchedule4(splitData[4]);

				scheduleList.add(timetable);
			}
			tableModel.setList(scheduleList);
			table.setModel(tableModel);

		} catch (Exception e) {
			e.printStackTrace();
		}

		table.setPreferredSize(new Dimension(1000, 150));
		table.getPreferredSize();
		panel41.add(new JScrollPane(table));
		panel41.setPreferredSize(new Dimension(1190, 150));
		panel41.getPreferredSize();

		table.setVisible(false);

		JButton book = new JButton("book");
		JPanel bkpne = new JPanel();
		bkpne.add(book);
		bkpne.setLayout(flowLayoutButton);
		bkpne.setPreferredSize(new Dimension(1190, 30));
		book.setVisible(false);

		// Add action listener to check button
		check.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				try {
					tableModel = new BookingTimeTable();

					FileInputStream bookingtt = new FileInputStream("src/database/BookingTimeTable.txt");
					BufferedReader tt = new BufferedReader(new InputStreamReader(bookingtt));

					FileInputStream bookingtt2 = new FileInputStream("Reports/Report2.txt");
					BufferedReader rr = new BufferedReader(new InputStreamReader(bookingtt2));

					boolean nameFound = false; 
					
					
					List<Timetable> scheduleList = new ArrayList<Timetable>();
					// br.readLine();//ignore first line
					
					while ((name1 = tt.readLine()) != null) {
						
						String[] splitData = name1.split("\\,");
						
						if (splitData.length > 1)
							name1 = splitData[1];
						name2 = splitData[2];
						name3 = splitData[3];
						name4 = splitData[4];

						if (name1.contains(doctorComboBox.getSelectedItem().toString())
								&& name1.contains(categoryComboBox.getSelectedItem().toString())
								|| name2.contains(doctorComboBox.getSelectedItem().toString())
										&& name2.contains(categoryComboBox.getSelectedItem().toString())
								|| name3.contains(doctorComboBox.getSelectedItem().toString())
										&& name3.contains(categoryComboBox.getSelectedItem().toString())
								|| name4.contains(doctorComboBox.getSelectedItem().toString())
										&& name4.contains(categoryComboBox.getSelectedItem().toString())) {
							nameFound = true;
							Timetable timetable = new Timetable();
							Date dateBegin = Date.valueOf(splitData[0]);
							timetable.setDay(dateBegin);
							timetable.setSchedule1(splitData[1]);
							timetable.setSchedule2(splitData[2]);
							timetable.setSchedule3(splitData[3]);
							timetable.setSchedule4(splitData[4]);

							scheduleList.add(timetable);
						}
					}
					if(nameFound == false) {
						new Options_Pane("Not Found!",
								"Slots For " + doctorComboBox.getSelectedItem() + " have been fully booked", 1);
					}
					tableModel.setList(scheduleList);
					table.setModel(tableModel);
					table.setVisible(true);
					table.setRowSelectionAllowed(false);
					table.setCellSelectionEnabled(true);

					rr.close();
					tt.close();
				
				}
				catch (FileNotFoundException ex) {
					new Options_Pane("No Selection!",
							"Please choose values to search", 1);
				} catch (IOException ex) {
					new Options_Pane("No Selection!",
							"Please choose values to search", 1);
				}

			}
		});
		
		table.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
			
			private static final long serialVersionUID = 6427523069214157126L;

			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int col) {

				Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
						col);

				if (table.getValueAt(row, col).toString().contains(doctorComboBox.getSelectedItem().toString())) {
					setBackground(Color.WHITE);
					table.setRowHeight(20);

				} else {
					c.setBackground(Color.white);
				}
				if (!(table.getValueAt(row, col).toString().contains(doctorComboBox.getSelectedItem().toString()))
						&& col > 0) {
					((JLabel) c).setText("");

				}

				return this;
			}
		});

		// add action listener to jTable
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				if (e.getClickCount() >= 1) {

					Date textField = (Date) table.getValueAt(table.getSelectedRow(), 0);
					book.setVisible(true);
					model.setValue(textField);
					selectedValue = (Date)table.getValueAt(table.getSelectedRow(), 0);
					int row = table.getSelectedRow();
					int col = table.getSelectedColumn();
					selectedCell = (String) table.getValueAt(row, col);
					appointment_time = table.getColumnName(col);
					
				}
			}
		});

		// Add Action listenr to submit button
		book.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					
					File file = new File("Reports/Report2.txt");
					File appointmnents = new File("src/database/Appointments.txt");
					File app = new File("src/database/BookingTimeTable.txt");
					String oldvalue = "";
					String line3;
					BufferedReader del= new BufferedReader(new FileReader(app));
					while ((line3 =del.readLine()) != null) 
					{
						oldvalue = oldvalue + line3 + System.lineSeparator(); continue;
				        //line3 = del.readLine();
					}
					String newContent = oldvalue.replace(selectedCell, "booked");
					FileWriter wrf = new FileWriter("src/database/BookingTimeTable.txt");
					wrf.write(newContent);
					del.close();
					wrf.close();
					

					if (!file.exists()) {
						file.createNewFile();
					}
					//update the reports file
					FileWriter fw = new FileWriter(file, true);
					BufferedWriter bw = new BufferedWriter(fw);
					PrintWriter pw = new PrintWriter(bw);
					
					DateFormat df = new SimpleDateFormat("yyyy-mm-dd");
					String s = df.format(selectedValue);
					
					pw.println("");
					pw.write(new_bookingTxt.getText().toString());
					pw.print(",");
					pw.write(visitornametxt.getText().toString());
					pw.print(",");
					pw.write(categoryComboBox.getSelectedItem().toString());
					pw.print(",");
					pw.write(doctorComboBox.getSelectedItem().toString());
					pw.print(",");
					pw.write(selectedValue.toString());
					pw.print(",");
					pw.write(appointment_time);
					pw.print(",");
					pw.write("booked (Visitor)");
					pw.close();
					
					//update the appointments file
					FileWriter fw1 = new FileWriter(appointmnents, true);
					BufferedWriter bw1 = new BufferedWriter(fw1);
					PrintWriter pw1 = new PrintWriter(bw1);
					pw1.println("");
					pw1.write(new_bookingTxt.getText().toString());
					pw1.print(",");
					pw1.write(visitornametxt.getText().toString());
					pw1.print(",");
					pw1.write(categoryComboBox.getSelectedItem().toString());
					pw1.print(",");
					pw1.write(doctorComboBox.getSelectedItem().toString());
					pw1.print(",");
					pw1.write(selectedValue.toString());
					pw1.print(",");
					pw1.write(appointment_time);
					pw1.print(",");
					pw1.write("booked by (Visitor)");
					pw1.close();
					
					

					new Options_Pane("Success!", "Information successfully added", 1);
					
				    VisitorMenu main = new VisitorMenu(); 
				    main.setVisible(true);
					Component book = (Component) e.getSource();
					Window bk = SwingUtilities.windowForComponent(book);
					bk.setVisible(false);
					//new_bookingTxt.setText("");
					//visitornametxt.setText("");
					///model.setSelected(false);
					//table.setVisible(false);
					//doctorComboBox.removeAllItems();
				} catch (IOException ioe) {
					
				    new Options_Pane("Error!", "An Error ocurred", 1);
					
					ioe.printStackTrace();
				}
			}

		});
		
		
		//changing appointmentID
				try { 

					File appid = new File("src/database/Appointments.txt");//load file for I/)
					Scanner sc = new Scanner(appid);
					int count = 0;
					while(sc.hasNextLine())
					{
						sc.nextLine();
						count++;	
					}
					int counter = count +1;
					String newid = Integer.toString(counter);
					new_bookingTxt.setText(newid);
					new_bookingTxt.setEditable(false);
					
					//close scanner 
					sc.close();

				} catch (Exception e) {
					e.printStackTrace();
				}



		// add components to the frame
		this.setLayout(flowLayoutButton);
		this.getContentPane().add(guipanel, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.add(panel2, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0));
		this.add(panel3, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0));
		this.add(booking, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0));
		this.add(panel4, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0));
		this.add(panel41, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0));
		this.add(bkpne, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0));
	}

}
