package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class DoctorDetails extends Settings {

	private static final long serialVersionUID = 1849528254055376524L;
	GUI_panel app;
	DoctorMenu docmenu;
	public GridBagLayout gridBagLayoutAppFrame;
	FlowLayout flowLayoutButton;
	GridLayout gridlayout;

	private JLabel idlbl, namelbl, addresslbl, mobilelbl, maillbl, detailsH;
	public JTextField idtxt, nametxt, mobiletxt, mailtxt;
	public JTextArea addresstxt;
	private JButton update;

	private final int columns = 2;
	private final int rows = 5;
	private final int vGap = 8;
	private final int hGap = 10;

	JPanel panel1, panel2, panel3;

	public DoctorDetails() {
		setupGUI();
	}

	private void setupGUI() {

		app = new GUI_panel();
		app.setPreferredSize(new Dimension (1190, 200));
		docmenu = new DoctorMenu();

		flowLayoutButton = new FlowLayout();
		flowLayoutButton.setAlignment(FlowLayout.CENTER);

		panel1 = new JPanel();
		detailsH= new JLabel("<HTML><U>Details</U></HTML>");// creates heading "details"
		detailsH.setForeground(Color.BLUE);
		detailsH.setFont(new Font("Serif", Font.PLAIN, 20));
		panel1.add(detailsH);
		panel1.setPreferredSize(new Dimension(1190, 30));
		panel1.setLayout(flowLayoutButton);

		panel2 = new JPanel();
		//labels
		idlbl = new JLabel("ID :");
		namelbl = new JLabel("Name :");
		addresslbl = new JLabel("Address :");
		mobilelbl = new JLabel("Mobile :");
		maillbl = new JLabel("Email Address :");
		//textFields
		idtxt = new JTextField();
		nametxt = new JTextField();
		mobiletxt= new JTextField();
		mailtxt = new JTextField();
		//textArea
		addresstxt = new JTextArea();

		gridlayout = new GridLayout(rows, columns);
		panel2.add(idlbl);
		panel2.add(idtxt);
		panel2.add(namelbl);
		panel2.add(nametxt);
		panel2.add(addresslbl);
		panel2.add(addresstxt);
		panel2.add(mobilelbl);
		panel2.add(mobiletxt);
		panel2.add(maillbl);
		panel2.add(mailtxt);


		panel2.setLayout(gridlayout);
		gridlayout.setVgap(vGap);
		gridlayout.setHgap(hGap);

		panel3 = new JPanel();
		update = new JButton("Update Details");
		panel3.add(update);
		panel3.setPreferredSize(new Dimension(1190, 50));
		panel3.setLayout(flowLayoutButton);




		this.setLayout(flowLayoutButton);
		this.getContentPane().add(app, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(docmenu.panel2, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.add(docmenu.doctorname, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.SOUTHWEST,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(panel1, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(panel2, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(panel3, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

		}
	}
