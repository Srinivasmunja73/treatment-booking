package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import database.Appointment;
import database.AppointmentsTableModel;

public class DoctorAppointments extends Settings{

	private static final long serialVersionUID = 1012580369989682827L;
	GUI_panel app;
	DoctorMenu doctormenu;
	PatientMenu patmenu;
	VisitorMenu vismenu;
    PatientCancelBooking patientmenu;
    FlowLayout flowLayoutButton;
    GridLayout gridlayout;
	GridBagLayout gridBagLayoutMainPanel;
	private JTable table;
	AppointmentsTableModel tableModel;
	JPanel panel2;

	public DoctorAppointments() {
		setupGUI();
	}

	private void setupGUI() {
		app = new GUI_panel();
		app.setPreferredSize(new Dimension (1190, 200));
		
        flowLayoutButton = new FlowLayout();
		flowLayoutButton.setAlignment(FlowLayout.CENTER);
		
		doctormenu = new DoctorMenu();
		
		//heading pane
		JPanel pane = new JPanel();// creates a new panel
		JLabel patientdetails= new JLabel("<HTML><U>My Appointments</U></HTML>");
		patientdetails.setForeground(Color.BLUE);
		patientdetails.setFont(new Font("Serif", Font.PLAIN, 20));
		//set panel properties and attributes
		pane.setPreferredSize(new Dimension(1190, 30));
		pane.add(patientdetails);
		pane.setLayout(flowLayoutButton);
		
		panel2 = new JPanel();
		table = new JTable();
		try { 

			FileInputStream doctorBookings = new FileInputStream("src/database/Appointments.txt");//load file for I/)
			BufferedReader bookings = new BufferedReader(new InputStreamReader(doctorBookings));
			String line;
			DoctorMenu docmenu = new DoctorMenu();
			String name = docmenu.namelbl.getText();
			
			patmenu = new PatientMenu();
			vismenu = new VisitorMenu();

			tableModel = new AppointmentsTableModel();
			List<Appointment> appointmetList = new ArrayList<Appointment>();
			//add data to treatment history table
			while((line = bookings.readLine()) != null)
			{
				String[] splitData = line.split(",");
				if (splitData.length>1)
					line = splitData[3];
				
				if(line.equalsIgnoreCase(name))
				{
					Appointment app = new Appointment ();
					app.setId(splitData[0]);
					app.setName(splitData[1]);
					app.setPhysician(splitData[3]);
					app.setTratment(splitData[2]);
					app.setDate(splitData[4]);
					app.setStatus(splitData[6]);
					app.setAppointment_time(splitData[5]);

					appointmetList.add(app);
				}
			}
			tableModel.setList(appointmetList);
			table.setModel(tableModel);		
			bookings.close();
			//filter data in table using jtextfield

		} catch (Exception e) {
			e.printStackTrace();
		}


		table.setPreferredSize(new Dimension (700,200));
		table.getPreferredSize();
		panel2.add(new JScrollPane(table));
		panel2.setSize(1190, 200);
		panel2.setPreferredSize(new Dimension(1190,200));

		

		
        //add components to the frame
		this.setLayout(flowLayoutButton);
		this.getContentPane().add(app, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(doctormenu.panel2, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.add(doctormenu.doctorname, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.SOUTHWEST,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(pane, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(panel2, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}

}
