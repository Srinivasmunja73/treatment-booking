package gui;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Options_Pane {

	private JFrame jframe;
	private String errorMsg;
	private String title;
	private int user_choice;

	public Options_Pane(String title, String errorMsg, int type) {

		this.errorMsg = errorMsg;
		this.title = title;

		switch (type) {
		case 1:
			displayError();
			break;

		case 2:
			displayInfo();
			break;

		default:
			break;
		}
	}

	private void displayError() {
		JOptionPane.showMessageDialog(jframe, errorMsg, title, JOptionPane.ERROR_MESSAGE);
	}

	private void displayInfo() {
		JOptionPane.showMessageDialog(jframe, errorMsg, title, JOptionPane.INFORMATION_MESSAGE);
	}


	/**
	 * @return the user_choice
	 */
	public int getUser_choice() {
		return user_choice;
	}

	/**
	 * @param user_choice the user_choice to set
	 */
	public void setUser_choice(int user_choice) {
		this.user_choice = user_choice;
	}

}
