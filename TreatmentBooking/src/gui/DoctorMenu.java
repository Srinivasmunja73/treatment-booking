package gui;
import database.Patient;
import database.PatientTableModel;
import database.Treatment;
import database.TreatmentTableModel;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableRowSorter;

public class DoctorMenu extends Settings{

	private static final long serialVersionUID = 1L;
	GUI_panel app;
	PatientCancelBooking patientmenu;
	FlowLayout flowLayoutButton;
	GridLayout gridlayout;
	GridBagLayout gridBagLayoutMainPanel;

	private ImageIcon icon8, icon9, icon10, icon11, icon6;
	public  JButton my_appointments, view_patients, add_treatment, my_details, logout;
	public JPanel panel2, searchPatient, doctorname;
	int columns = 3;
	int rows = 1;
	int vGap = 4;
	public  JLabel patid;
	public JLabel namelbl; public JTextField searchbox; public JButton search;
	PatientTableModel tableModel;
	TreatmentTableModel tableModel2;

	TableRowSorter<PatientTableModel> sorter;
	TableRowSorter<TreatmentTableModel> sorter2;
	JTable table, table2;
	FileInputStream doctor;
	BufferedReader dr;
	public DoctorMenu() {
		setupGUI();
	}

	private void setupGUI() {
		app = new GUI_panel();
		app.setPreferredSize(new Dimension (1190, 150));

		flowLayoutButton = new FlowLayout();
		flowLayoutButton.setAlignment(FlowLayout.CENTER);

		gridBagLayoutMainPanel = new GridBagLayout();
		
		icon8  = new ImageIcon(getClass().getClassLoader().getResource("image2.png"));
		icon9  = new ImageIcon(getClass().getClassLoader().getResource("image9.png"));
		icon10 = new ImageIcon(getClass().getClassLoader().getResource("image10.png"));
		icon11 = new ImageIcon(getClass().getClassLoader().getResource("image11.png"));
		icon6 = new ImageIcon(getClass().getClassLoader().getResource("image7.png"));

		my_details= new JButton(icon8);
		my_details.setBackground(Color.WHITE);
		my_details.setOpaque(false);
		my_details.setBorder(null);

		my_details.addActionListener(new ActionListener()//add action listener to mydetails button
				{
			@Override
			public void actionPerformed(ActionEvent e) {
				DoctorDetails log = new DoctorDetails();
				log.setVisible(true);
				//close the current window
				Component my_details = (Component)e.getSource();
				Window bk = SwingUtilities.windowForComponent(my_details);
				bk.setVisible(false);
				try {
					doctor = new FileInputStream("src/database/doctor.txt");//load file for I/)
					dr = new BufferedReader(new InputStreamReader(doctor)) ;
					String line, name;

					while((line = dr.readLine()) !=null)
					{
						String jlabelText = namelbl.getText();
						String[] tokens = line.split(",");
						if(tokens.length>1)
							line = tokens[0];
						name = tokens[1];
						//pos = pos + 1;
						if(name.equalsIgnoreCase(jlabelText))
						{	
							log.idtxt.setText(line);
							log.nametxt.setText(name);
							log.mobiletxt.setText(tokens[2]);
							log.mailtxt.setText(tokens[3]);
							log.addresstxt.setText(tokens[4]);

						}
					}	
					dr.close(); 	
				}

				catch (FileNotFoundException ex)
				{
					Logger.getLogger(DoctorMenu.class.getName()).log(Level.SEVERE, null, ex);
				}
				catch (IOException ex)
				{
					Logger.getLogger(DoctorMenu.class.getName()).log(Level.SEVERE, null, ex);
				}

			}

				});

		my_appointments= new JButton(icon9);
		my_appointments.setBackground(Color.WHITE);
		my_appointments.setOpaque(false);
		my_appointments.setBorder(null);

		my_appointments.addActionListener(new ActionListener()// add action listener to my appointments button
				{

			@Override
			public void actionPerformed(ActionEvent e) {
				DoctorAppointments log = new DoctorAppointments();
				log.setVisible(true);
				//close the current window
				Component my_appointments= (Component)e.getSource();
				Window bk = SwingUtilities.windowForComponent(my_appointments);
				bk.setVisible(false);

			}

				});

		view_patients= new JButton(icon10);
		view_patients.setBackground(Color.WHITE);
		view_patients.setOpaque(false);
		view_patients.setBorder(null);

		view_patients.addActionListener(new ActionListener()// add action listener to view patient button
				{

			@Override
			public void actionPerformed(ActionEvent e) {
				DoctorMenu log = new DoctorMenu();
				log.setVisible(true);
				//close the current window
				Component view_patients= (Component)e.getSource();
				Window bk = SwingUtilities.windowForComponent(view_patients);
				bk.setVisible(false);

			}

				});

		add_treatment= new JButton(icon11);
		add_treatment.setBackground(Color.WHITE);
		add_treatment.setOpaque(false);
		add_treatment.setBorder(null);
		add_treatment.addActionListener(new ActionListener()// add action listener to view patient button
				{

			@Override
			public void actionPerformed(ActionEvent e) {
				AddPatient log = new AddPatient();
				log.setVisible(true);
				//close the current window
				Component add_treatment= (Component)e.getSource();
				Window bk = SwingUtilities.windowForComponent(add_treatment);
				bk.setVisible(false);

			}

				});

		logout= new JButton(icon6);
		logout.setBackground(Color.WHITE);
		logout.setOpaque(false);
		logout.setBorder(null);

		logout.addActionListener(new ActionListener()//add action listener to log out button
				{
			@Override
			public void actionPerformed(ActionEvent e) {
				App_GUI log = new App_GUI();
				log.setVisible(true);
				//close the current window
				Component logout = (Component)e.getSource();
				Window bk = SwingUtilities.windowForComponent(logout);
				bk.setVisible(false);	
			}

				});

		//add functionalities to buttons
		panel2 = new JPanel(); // panel 2

		//panel 2 attributes
		panel2.setLayout(flowLayoutButton);
		panel2.setPreferredSize(new Dimension(1190, 50));
		panel2.setBackground(Color.BLUE);
		panel2.add(my_details);
		panel2.add(my_appointments);
		panel2.add(view_patients);
		panel2.add(add_treatment);
		panel2.add(logout);

		//panel3
		JPanel panel3 = new JPanel();// creates a new panel
		JLabel patientdetails= new JLabel("<HTML><U>Patient Details</U></HTML>");
		patientdetails.setForeground(Color.BLUE);
		patientdetails.setFont(new Font("Serif", Font.PLAIN, 20));
		//set panel properties and attributes
		panel3.setPreferredSize(new Dimension(1190, 30));
		panel3.add(patientdetails);
		panel3.setLayout(flowLayoutButton);

		searchPatient = new JPanel();
		patid = new JLabel("Patient ID"); searchbox = new JTextField(); search = new JButton("Search");
		searchbox.setPreferredSize(new Dimension(100, 26));
		searchbox.setSize(getPreferredSize());
		gridlayout = new GridLayout(rows, columns);
		searchPatient.add(patid);
		searchPatient.add(searchbox);
		searchPatient.add(search);

		searchPatient.setLayout(gridlayout);
		gridlayout.setHgap(vGap);
		searchPatient.setSize(1190,  60);

		JPanel pane = new JPanel();
		JLabel treatmentH= new JLabel("<HTML><U>Patient Treatment History</U></HTML>");
		treatmentH.setForeground(Color.BLUE);
		treatmentH.setFont(new Font("Serif", Font.PLAIN, 20));
		//set panel properties and attributes
		pane.setPreferredSize(new Dimension(1190, 30));
		pane.add(treatmentH);
		pane.setLayout(flowLayoutButton);

		//create a panel for patients details
		JPanel panel4 = new JPanel();
		table = new JTable();
		try { 

			tableModel = new PatientTableModel();
			FileInputStream patient = new FileInputStream("src/database/patient.txt");//load file for I/)
			BufferedReader br = new BufferedReader(new InputStreamReader(patient));
			String line; 
			List<Patient> patientList = new ArrayList<Patient>();
			//add data to patient details table
			while((line = br.readLine()) != null)
			{
				String[] splitData = line.split(",");
				// add patient details to the table
				Patient patient1 = new Patient();
				patient1.setId(splitData[0]);
				patient1.setName(splitData[1]);
				patient1.setAddress(splitData[2]);
				patient1.setMobile(splitData[3]);
				patient1.setEmail(splitData[4]);

				patientList.add(patient1);
			}
			tableModel.setList(patientList);
			table.setModel(tableModel);	

			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		table.setPreferredSize(new Dimension (600,70));
		panel4.add(new JScrollPane(table));
		panel4.setSize(1190, 100);
		panel4.setPreferredSize(new Dimension(1190,100));
		sorter = new TableRowSorter<>(tableModel);
		table.setRowSorter(sorter);

		// get treatment history data
		JPanel treatmetHistory = new JPanel();
		table2 = new JTable();
		try { 

			FileInputStream treatment = new FileInputStream("Reports/Report1.txt");//load file for I/)
			BufferedReader tr = new BufferedReader(new InputStreamReader(treatment));
			String line;
			tableModel2 = new TreatmentTableModel();
			List<Treatment> treatmentList = new ArrayList<Treatment>();
			//add data to treatment history table
			while((line = tr.readLine()) != null)
			{
				String[] splitData = line.split(",");
				Treatment treatment1 = new Treatment();
				treatment1.setPatientid(splitData[0]);
				treatment1.setName(splitData[1]);
				treatment1.setTreatment(splitData[2]);
				treatment1.setRoom(splitData[3]);
				treatment1.setDate(splitData[4]);
				treatment1.setPhysician(splitData[5]);

				treatmentList.add(treatment1);

			}
			tableModel2.setList(treatmentList);
			table2.setModel(tableModel2);		
			tr.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		table2.setPreferredSize(new Dimension (700,200));
		table2.getPreferredSize();
		sorter2 = new TableRowSorter<>(tableModel2);
		table2.setRowSorter(sorter2);
		treatmetHistory .add(new JScrollPane(table2));
		treatmetHistory .setSize(1190, 200);
		treatmetHistory.setPreferredSize(new Dimension(1190,200));


		doctorname = new JPanel();// creates a new panel for doctors name label
		namelbl= new JLabel("<HTML><span>Doctor's Name</span></HTML>");
		namelbl.setForeground(Color.BLACK);
		namelbl.setFont(new Font("Serif", Font.PLAIN, 15));
		//set panel properties and attributes
		doctorname .setPreferredSize(new Dimension(1190, 30));
		namelbl.setText(String.valueOf(DbLogin.token1[1]));
		doctorname .add(namelbl);
		doctorname .setLayout(flowLayoutButton);
		



		search.addActionListener(new ActionListener()// add action listener 
				{   
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				String text = searchbox.getText();
				if (text.length() == 0) {
					sorter.setRowFilter(null);
					sorter2.setRowFilter(null);
				} else {
					sorter.setRowFilter(RowFilter.regexFilter(text));
					sorter2.setRowFilter(RowFilter.regexFilter(text));
				}
			}

				});


		// add cation listener to search button
		searchbox.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				search(searchbox.getText().toString());
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
				search(searchbox.getText().toString());
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				search(searchbox.getText().toString());
			}
			public void search(String str) {
				if (str.length() == 0) {
					sorter.setRowFilter(null);
					sorter2.setRowFilter(null);
				} else {
					sorter.setRowFilter(RowFilter.regexFilter(str));
					sorter2.setRowFilter(RowFilter.regexFilter(str));
				}
			}

		});

		//add components to the frame
		this.setLayout(flowLayoutButton);
		this.getContentPane().add(app, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.add(panel2, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		this.add(doctorname, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.SOUTHWEST,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		this.add(panel3, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		this.add(searchPatient, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		this.add(panel4, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		this.add(pane, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		this.add(treatmetHistory , new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
	}

}




