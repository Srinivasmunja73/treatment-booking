package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class DbLogin  extends JPanel implements ActionListener{

	private static final long serialVersionUID = 8326975012877537600L;
	private JLabel usernameLbl, signup;
	private JTextField username;
	private JButton submit;
	LoginPane logpane;

	GUI_panel guipanel;

	private final int columns = 2;
	private final int rows = 1;
	private final int vGap = 4;
	private final int hGap = 4;

	private final String userlblTxt = "UserId :";
	private final String buttonTxt = "SUBMIT";
	private final String signupText = "New user register?";
	private GridLayout gridLayoutLogin;
    public static String docname;
	public JPanel pane1,pane;
	
	FileInputStream patient,doctor;
	BufferedReader br, dr;
	public static String[] tokens;
	public static String[] token1;
	public DbLogin() {
		setupGUI();
	}

	private void setupGUI() {	
		
		usernameLbl = new JLabel(userlblTxt);
		usernameLbl.setPreferredSize(new Dimension(100, 24));
		usernameLbl.getPreferredSize();
		usernameLbl.setOpaque(true);

		username = new JTextField();
		username.setPreferredSize(new Dimension(150, 24));
		username.getPreferredSize();
		
		signup = new JLabel(signupText);
		signup.setForeground(Color.BLUE);
		signup.setAlignmentX(BOTTOM_ALIGNMENT);
		
		signup.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				PatientRegistration log = new PatientRegistration ();
        		log.setVisible(true);
        		//close the current window
        		Component signup = (Component)e.getSource();
        		Window logpane = SwingUtilities.windowForComponent(signup);
        		logpane.setVisible(false);
			}
		});
		
		submit = new JButton(buttonTxt);

		//this.add(guipanel);
		gridLayoutLogin = new GridLayout(rows, columns);
		pane1 = new JPanel();
		pane1.setLayout(gridLayoutLogin);
		pane1.add(usernameLbl);
		pane1.add(username);
		

		gridLayoutLogin.setVgap(vGap);
		gridLayoutLogin.setHgap(hGap);
		
		pane = new JPanel();
		//pane.setLayout(gridLayoutLogin);
		pane.add(submit);
		pane.add(signup);
		
		this.add(pane1);
		this.add(pane);
		
		
		submit.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) {

				try {
					patient = new FileInputStream("src/database/patient.txt");//load file for I/)
					br = new BufferedReader(new InputStreamReader(patient));
					doctor = new FileInputStream("src/database/doctor.txt");//load file for I/)
					dr = new BufferedReader(new InputStreamReader(doctor)) ;
                    String id = username.getText();
                    String line;
                    String line1;
                    boolean logIn = false;//check if patient is logged in

                    while((line = br.readLine()) !=null )
                    {
                    	tokens = line.split(",");
                    	if(tokens.length>1)
                    	line = tokens[0];
                        //pos = pos + 1;
                    	if(line.equalsIgnoreCase(id))
                    	{
                    		logIn = true;
                    		PatientMenu log = new PatientMenu ();
                    		log.setVisible(true);
                    		//close the current window
                    		Component submit = (Component)e.getSource();
                    		Window logpane = SwingUtilities.windowForComponent(submit);
                    		logpane.setVisible(false);
                            //cbk.patientmenu.namelbl.setText(tokens[1]);
                    		br.close();	
                    	}
                    	
                    }
                    
                    while((line1 = dr.readLine()) !=null)
                    {
                    	token1 = line1.split(",");
                    	if(token1.length>1)
                    		line1 = token1[0];
                    	if(line1.equalsIgnoreCase(id))
                    	{
                    		logIn =true;
                    		DoctorMenu log = new DoctorMenu();
                    		log.setVisible(true);
                    		//log.namelbl.setText(tokens[1]);
                    		//close the current window
                    		Component submit = (Component)e.getSource();
                    		Window logpane = SwingUtilities.windowForComponent(submit);
                    		logpane.setVisible(false);
                    		dr.close();
                    	}

                    	
                    }

                	if(logIn == false)
                    	{
                    		
                    		new Options_Pane("Invalid user ID!", "Please enter a correct user ID", 1);
                    		br.close();dr.close();
                    	}      	
                    	
				}
				 catch (FileNotFoundException ex)
			    {
			       Logger.getLogger(DbLogin.class.getName()).log(Level.SEVERE, null, ex);
			    }
			    catch (IOException ex)
			    {
			    	//Logger.getLogger(DbLogin.class.getName()).log(Level.SEVERE, null, ex);
			    }

			}
			
		});
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username.getText();
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

}
