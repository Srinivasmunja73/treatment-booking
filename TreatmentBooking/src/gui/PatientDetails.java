package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class PatientDetails extends Settings{

    GUI_panel app;
    public PatientMenu patientmenu;
    public GridBagLayout gridBagLayoutAppFrame;
    FlowLayout flowLayoutButton;
    GridLayout gridlayout;
	private static final long serialVersionUID = 2321026443416127333L;
	
	private JLabel idlbl, namelbl, addresslbl, mobilelbl, maillbl, detailsH;
	public JTextField idtxt, nametxt, mobiletxt, mailtxt;
	public JTextArea addresstxt;
	private JButton update;
	
	private final int columns = 2;
	private final int rows = 5;
	private final int vGap = 8;
	private final int hGap = 10;
	
	private String addr, mail, mobile;
	JPanel panel1, panel2, panel3;
	public PatientDetails()
	{
		setupGUI();
	}

	private void setupGUI() {
		
		app = new GUI_panel();
		app.setPreferredSize(new Dimension (1190, 200));
		patientmenu = new PatientMenu();
		
        flowLayoutButton = new FlowLayout();
		flowLayoutButton.setAlignment(FlowLayout.CENTER);
		
		panel1 = new JPanel();
		detailsH= new JLabel("<HTML><U>Details</U></HTML>");// creates heading "details"
		detailsH.setForeground(Color.BLUE);
		detailsH.setFont(new Font("Serif", Font.PLAIN, 20));
		panel1.add(detailsH);
		panel1.setPreferredSize(new Dimension(1190, 30));
		panel1.setLayout(flowLayoutButton);
		
		panel2 = new JPanel();
		//labels
		idlbl = new JLabel("ID :");
		namelbl = new JLabel("Name :");
		addresslbl = new JLabel("Address :");
		mobilelbl = new JLabel("Mobile :");
		maillbl = new JLabel("Email Address :");
		//textFields
		idtxt = new JTextField();
		idtxt.setPreferredSize(new Dimension(100,30));
		idtxt.getPreferredSize();
		idtxt.setEditable(false);
		nametxt = new JTextField();
		nametxt.setPreferredSize(new Dimension(100,30));
		nametxt.getPreferredSize();
		nametxt.setEditable(false);
		mobiletxt= new JTextField();
		mobiletxt.setPreferredSize(new Dimension(100,30));
		mobiletxt.getPreferredSize();
		mailtxt = new JTextField();
		mailtxt.setPreferredSize(new Dimension(100,30));
		mailtxt.getPreferredSize();
		//textArea
		addresstxt = new JTextArea();
		addresstxt.setPreferredSize(new Dimension(100,30));
		addresstxt.getPreferredSize();
		addr = addresstxt.getText();
		
		gridlayout = new GridLayout(rows, columns);
		panel2.add(idlbl);
		panel2.add(idtxt);
		panel2.add(namelbl);
		panel2.add(nametxt);
		panel2.add(addresslbl);
		panel2.add(addresstxt);
		panel2.add(mobilelbl);
		panel2.add(mobiletxt);
		panel2.add(maillbl);
		panel2.add(mailtxt);

	
		panel2.setLayout(gridlayout);
		gridlayout.setVgap(vGap);
		gridlayout.setHgap(hGap);
		
		panel3 = new JPanel();
		update = new JButton("Update Details");
		panel3.add(update);
		panel3.setPreferredSize(new Dimension(1190, 50));
		panel3.setLayout(flowLayoutButton);

	      addresstxt.addMouseListener(new MouseAdapter() {
	          public void mouseClicked(MouseEvent me) {
	        	  addr = addresstxt.getText();
	          }
	       });
	      mobiletxt.addMouseListener(new MouseAdapter() {
	          public void mouseClicked(MouseEvent me) {
	        	  mobile = mobiletxt.getText();
	          }
	       });
	      mailtxt.addMouseListener(new MouseAdapter() {
	          public void mouseClicked(MouseEvent me) {
	        	  mail = mailtxt.getText();
	          }
	       });
	
	
	
		update.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {

					File file = new File("src/database/patient.txt");
					String oldaddress = "";
					String oldmobile = "";
					String oldmail = "";
					String line4;
					BufferedReader del= new BufferedReader(new FileReader(file));
                    boolean detailInserted =false;
					while ((line4 =del.readLine()) != null) 
					{
						
						    oldaddress = oldaddress + line4 + System.lineSeparator(); continue;
					}
					while ((line4 =del.readLine()) != null) 
					{
						    oldmobile = oldmobile + line4 + System.lineSeparator();continue;
					}
					while ((line4 =del.readLine()) != null) 
					{
						    oldmail = oldmobile + line4 + System.lineSeparator();continue;
					}
					detailInserted = true;
					String newaddress = oldaddress.replace(addr, addresstxt.getText());
					String newmobile = oldmobile.replace(mobile, mobiletxt.getText());
					String newmail = oldmail.replace(mail, mailtxt.getText());
					FileWriter wrf = new FileWriter("src/database/patient.txt");
					wrf.write(newaddress);
					wrf.write(newmobile);
					wrf.write(newmail);
					del.close();
					wrf.close();
					
					new Options_Pane("Success!","Details updates Successfully", 1);
					
                    
					
					if (detailInserted == false)
					{
						new Options_Pane("Error!","Please update all fields (address, mobile, mail)", 1);
					}
					

				} catch (IOException ioe) {
					new Options_Pane("Error!","Please update all fields (address, mobile, mail)", 1);
					
				}
			}

		});
		
		this.setLayout(flowLayoutButton);
		this.getContentPane().add(app, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(patientmenu.panel2, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(patientmenu.patientname, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(panel1, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(panel2, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(panel3, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		
	}
}
