package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class Buttons extends JPanel {


	private static final long serialVersionUID = 5189390802374729942L;
	private JButton patientLogBtn;
	private JButton doctorLogBtn;
	private JButton visitorLogBtn;
	private FlowLayout flowLayoutButton;
	App_GUI app;


	public Buttons() {
	
		Icon icon  = new ImageIcon(getClass().getClassLoader().getResource("patient.png"));
		Icon icon1  = new ImageIcon(getClass().getClassLoader().getResource("doctor.png"));
		Icon icon2  = new ImageIcon(getClass().getClassLoader().getResource("visitor.png"));

		patientLogBtn= new JButton(icon);
	    patientLogBtn.setBackground(Color.WHITE);
	    patientLogBtn.setOpaque(false);
	    patientLogBtn.setBorder(null);
	    
		doctorLogBtn = new JButton(icon1);
		doctorLogBtn.setBackground(Color.WHITE);
		doctorLogBtn.setEnabled(true);
		doctorLogBtn.setOpaque(false);
		doctorLogBtn.setBorder(null);

		visitorLogBtn = new JButton(icon2);
		visitorLogBtn.setBackground(Color.WHITE);
		visitorLogBtn.setEnabled(true);
		visitorLogBtn.setOpaque(false);
		visitorLogBtn.setBorder(null);

		flowLayoutButton = new FlowLayout();
		flowLayoutButton.setAlignment(FlowLayout.LEADING);

		this.setLayout(flowLayoutButton);
		this.setPreferredSize(new Dimension(800, 100));
		this.setBackground(Color.WHITE);
		this.add(patientLogBtn);
		this.add(doctorLogBtn);
		this.add(visitorLogBtn);
		
		//add actionListener to patient login button
		patientLogBtn.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e) {
				LoginPane log = new LoginPane();
				log.setVisible(true);
				//close the current window
				 Component patientLogBtn = (Component)e.getSource();
				 Window app = SwingUtilities.windowForComponent(patientLogBtn);
				 app.setVisible(false);
				
			}
			
		});
		doctorLogBtn.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e) {
				LoginPane log = new LoginPane();
				log.setVisible(true);
				//close the current window
				 Component patientLogBtn = (Component)e.getSource();
				 Window app = SwingUtilities.windowForComponent(patientLogBtn);
				 app.setVisible(false);
				
			}
			
		});
		visitorLogBtn.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e) {
				VisitorMenu log = new VisitorMenu();
				log.setVisible(true);
				//close the current window
				 Component visitorLogBtn = (Component)e.getSource();
				 Window app = SwingUtilities.windowForComponent(visitorLogBtn);
				 app.setVisible(false);
				
			}
			
		});
	}

}
