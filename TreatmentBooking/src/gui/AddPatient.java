package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

public class AddPatient extends Settings {

	private static final long serialVersionUID = 4420731784346913940L;
	GUI_panel app;
	DoctorMenu doctormenu;
	PatientCancelBooking patientmenu;
	FlowLayout flowLayoutButton;
	GridLayout gridlayout;
	GridBagLayout gridBagLayoutMainPanel;
	
	private JTextField nametxt, treatmenttxt;
	private JComboBox<String> roomcombobox;

	UtilDateModel model = new UtilDateModel();
	private final int columns = 2;
	private final int rows = 4;
	private final int vGap = 3;
	private final int hGap = 1;

	public AddPatient() {
		setupGUI();
	}

	private void setupGUI() {
		app = new GUI_panel();
		app.setPreferredSize(new Dimension (1190, 150));

		flowLayoutButton = new FlowLayout();
		flowLayoutButton.setAlignment(FlowLayout.CENTER);

		//heading pane
		JPanel pane = new JPanel();// creates a new panel
		JLabel patientdetails= new JLabel("<HTML><U>Add Treatment</U></HTML>");
		patientdetails.setForeground(Color.BLUE);
		patientdetails.setFont(new Font("Serif", Font.PLAIN, 20));
		//set panel properties and attributes
		pane.setPreferredSize(new Dimension(1190, 30));
		pane.add(patientdetails);
		pane.setLayout(flowLayoutButton);
		

		//create addtreatment panel
		JPanel treatment = new JPanel();
		JLabel name = new JLabel("Name :");
		JLabel treatmentFor = new JLabel("Treatment For :");
		JLabel room = new JLabel("Room :");
		JLabel datelbl = new JLabel("Date_Time :");

		
		nametxt = new JTextField();
		treatmenttxt = new JTextField();

		//categoryComboBox, doctorComboBox;
		roomcombobox = new JComboBox<String>();
		roomcombobox.addItem("Consulting suite A");
		roomcombobox.addItem("Consulting suite B");
		roomcombobox.addItem("Consulting suite C");
		roomcombobox.addItem("Swimming Pool");
		roomcombobox.addItem("Gym");
		
		//addalender
		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");
		JDatePanelImpl datePanel = new JDatePanelImpl(model, p);
		JDatePickerImpl datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());

		//add components to treatment panel
		gridlayout= new GridLayout(rows, columns);
		treatment.add(name);
		treatment.add(nametxt);
		treatment.add(treatmentFor);
		treatment.add(treatmenttxt);
		treatment.add(room);
		treatment.add(roomcombobox);
		treatment.add(datelbl);
		treatment.add(datePicker);
		


		treatment.setLayout(gridlayout);
		gridlayout.setVgap(vGap);
		gridlayout.setHgap(hGap);
		
		doctormenu = new DoctorMenu();
		doctormenu.searchPatient.setLayout(flowLayoutButton);
		doctormenu.searchPatient.setPreferredSize(new Dimension(1190, 50));
		
		//add action listener to search button
		doctormenu.search.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {

				try {
					FileInputStream patient = new FileInputStream("src/database/patient.txt");//load file for I/O
					BufferedReader br = new BufferedReader(new InputStreamReader(patient));
					String line, name; boolean patientFound = false;

					String id = doctormenu.searchbox.getText();
					while((line = br.readLine()) !=null)
					{
						
						String[] tokens = line.split(",");
						if(tokens.length>1)
							line = tokens[0];
						name = tokens[0];
						//pos = pos + 1;
						if(name.equalsIgnoreCase(id))
						{
							patientFound =true;
							nametxt.setText(tokens[1]);
							
						}
						
					}	
					br.close();
					
					if(patientFound == false)
					{
						new Options_Pane("Invalid Input!", "A patient with id"+id+" does not exist", 1);
						br.close();
					}
				}

				catch (FileNotFoundException ex)
				{
					Logger.getLogger(AddPatient.class.getName()).log(Level.SEVERE, null, ex);
				}
				catch (IOException ex)
				{
					Logger.getLogger(AddPatient.class.getName()).log(Level.SEVERE, null, ex);
				}
			}

		});

		
		//Panel 4 containing the SUBMIT button
				JPanel panel4 = new JPanel();// creates a new panel
				JButton submit = new JButton("SUBMIT");
				//set panel properties and attributes
				panel4.setPreferredSize(new Dimension(1190, 50));
				panel4.add(submit);
				panel4.setLayout(flowLayoutButton);
				
				//Add Action listenr to submit button
				submit.addActionListener(new ActionListener(){
					@SuppressWarnings("deprecation")
					@Override
					public void actionPerformed(ActionEvent e) 
					{
						 try{
					          File file =new File("Reports/Report1.txt");
					    	  if(!file.exists()){
					    	 	file.createNewFile();
					    	  }
					    	  FileWriter fw = new FileWriter(file,true);
					    	  BufferedWriter bw = new BufferedWriter(fw);
					    	  PrintWriter pw = new PrintWriter(bw);
					    	  pw.println("");
					    	  pw.write(doctormenu.searchbox.getText());
					    	  pw.print(",");
					    	  pw.write(nametxt.getText());
					    	  pw.print(",");
					    	  pw.write(treatmenttxt.getText());
					    	  pw.print(",");
					    	  pw.write(roomcombobox.getSelectedItem().toString());
					    	  pw.print(",");
					    	  pw.write(model.getValue().toGMTString());
					    	  pw.print(",");
					    	  pw.write(doctormenu.namelbl.getText().toString());
					    	  pw.write("  :Appointment Attended");
					    	  pw.close();

					    	  new Options_Pane("Success!", "Information successfully added", 1);
					    	  doctormenu.searchbox.setText("");
					    	  nametxt.setText("");
					    	  treatmenttxt.setText("");
					    	  model.setSelected(false);
					    	  doctormenu.table.removeAll();
					       }catch(IOException ioe){
					    	   System.out.println("Exception occurred:");
					    	   ioe.printStackTrace();
					      }
					}

				});

				

 
		//add components to the frame
		this.setLayout(flowLayoutButton);
		this.getContentPane().add(app, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(doctormenu.panel2, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.add(doctormenu.doctorname, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.SOUTHWEST,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(pane, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(doctormenu.searchPatient, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(treatment, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(panel4, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

	}

}
