package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

public class GUI_panel extends JPanel{


	private static final long serialVersionUID = -6280175950506653257L;
	GridBagLayout gridBagLayoutMainPanel;
	private ImageIcon icon;
	

		public GUI_panel() {
			setupGUI();
		}

		private void setupGUI() {

			gridBagLayoutMainPanel = new GridBagLayout();
    		icon  = new ImageIcon(getClass().getClassLoader().getResource("image1.jpg"));
			
			JPanel panel = new JPanel()
			{
				private static final long serialVersionUID = 1L;

				protected void paintComponent(Graphics g)
				{
					g.drawImage(icon.getImage(), 0,0, this.getWidth(), this.getHeight(), null);
					super.paintComponent(g);
				}
			};
			
			panel.setOpaque(false);
			panel.setPreferredSize(new Dimension(1190, 350));
			TitledBorder titledBorder = BorderFactory.createTitledBorder("Physiotherapy & Sports Injury Centre Treatment Booking");
		    titledBorder.setTitleJustification(TitledBorder.CENTER);
		    titledBorder.setTitleFont(new Font ("Tahoma",Font.BOLD, 20));;
			titledBorder.setTitleColor(Color.darkGray);
			panel.setBorder(titledBorder);
			this.add(panel);
			

		}

}
