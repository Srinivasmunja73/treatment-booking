package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class PatientRegistration extends Settings{

    GUI_panel app;
    public PatientMenu patientmenu;
    public GridBagLayout gridBagLayoutAppFrame;
    FlowLayout flowLayoutButton;
    GridLayout gridlayout;
	private static final long serialVersionUID = 2321026443416127333L;
	
	private JLabel idlbl, namelbl, addresslbl, mobilelbl, maillbl, detailsH;
	public JTextField idtxt, nametxt, mobiletxt, mailtxt;
	public JTextArea addresstxt;
	private JButton signup;
	
	private final int columns = 2;
	private final int rows = 5;
	private final int vGap = 8;
	private final int hGap = 10;
	
	JPanel panel1, panel2, panel3;
	public PatientRegistration()
	{
		setupGUI();
	}

	private void setupGUI() {
		
		app = new GUI_panel();
		app.setPreferredSize(new Dimension (1190, 200));
		
        flowLayoutButton = new FlowLayout();
		flowLayoutButton.setAlignment(FlowLayout.CENTER);
		
		panel1 = new JPanel();
		detailsH= new JLabel("<HTML><U>Sign Up</U></HTML>");// creates heading "details"
		detailsH.setForeground(Color.BLUE);
		detailsH.setFont(new Font("Serif", Font.PLAIN, 20));
		panel1.add(detailsH);
		panel1.setPreferredSize(new Dimension(1190, 30));
		panel1.setLayout(flowLayoutButton);
		
		panel2 = new JPanel();
		//labels
		idlbl = new JLabel("ID :");
		namelbl = new JLabel("Name :");
		addresslbl = new JLabel("Address :");
		mobilelbl = new JLabel("Mobile :");
		maillbl = new JLabel("Email Address :");
		//textFields
		idtxt = new JTextField();
		idtxt.setPreferredSize(new Dimension(100,30));
		idtxt.getPreferredSize();
		idtxt.setEditable(false);
		nametxt = new JTextField();
		nametxt.setPreferredSize(new Dimension(100,30));
		nametxt.getPreferredSize();
		mobiletxt= new JTextField();
		mobiletxt.setPreferredSize(new Dimension(100,30));
		mobiletxt.getPreferredSize();
		mailtxt = new JTextField();
		mailtxt.setPreferredSize(new Dimension(100,30));
		mailtxt.getPreferredSize();
		//textArea
		addresstxt = new JTextArea();
		addresstxt.setPreferredSize(new Dimension(100,30));
		addresstxt.getPreferredSize();
		
		gridlayout = new GridLayout(rows, columns);
		panel2.add(idlbl);
		panel2.add(idtxt);
		panel2.add(namelbl);
		panel2.add(nametxt);
		panel2.add(addresslbl);
		panel2.add(addresstxt);
		panel2.add(mobilelbl);
		panel2.add(mobiletxt);
		panel2.add(maillbl);
		panel2.add(mailtxt);

	
		panel2.setLayout(gridlayout);
		gridlayout.setVgap(vGap);
		gridlayout.setHgap(hGap);
		
		panel3 = new JPanel();
		signup = new JButton("Sign Up");
		panel3.add(signup);
		panel3.setPreferredSize(new Dimension(1190, 50));
		panel3.setLayout(flowLayoutButton);
		
		//changing appointmentID
		try { 
			
			FileInputStream patient = new FileInputStream("src/database/patient.txt");//load file for I/)
			BufferedReader br = new BufferedReader(new InputStreamReader(patient)) ;
			String line;

			while((line = br.readLine()) !=null)
			{
				String[] tokens = line.split(",");
				if(tokens.length>1)
					line = tokens[0];
				
				int id = Integer.parseInt(line);
				int counter = id + 1;
				String newid = Integer.toString(counter);
				idtxt.setText(newid);
				idtxt.setEditable(false);
			}	
			br.close(); 
		} catch (Exception e) {
			e.printStackTrace();
		}

		
		signup.addActionListener(new ActionListener() {
			@Override
					public void actionPerformed(ActionEvent e) 
					{
						 try{
							 File file = new File("src/database/patient.txt");
					    	  if(!file.exists()){
					    	 	file.createNewFile();
					    	  }
					    	  FileWriter fw = new FileWriter(file,true);
					    	  BufferedWriter bw = new BufferedWriter(fw);
					    	  PrintWriter pw = new PrintWriter(bw);
					    	  pw.println("");
					    	  pw.write(idtxt.getText());
					    	  pw.print(",");
					    	  pw.write(nametxt.getText());
					    	  pw.print(",");
					    	  pw.write(addresstxt.getText());
					    	  pw.print(",");
					    	  pw.write(mobiletxt.getText());
					    	  pw.print(",");
					    	  pw.write(mailtxt.getText());
					    	  pw.close();

					    	  new Options_Pane("Success!", "Registration Successful!", 1);
					    	  //idtxt.setText("");
					    	  nametxt.setText("");
					    	  addresstxt.setText("");
					    	  mobiletxt.setText("");
					    	  mailtxt.setText("");
					       }catch(IOException ioe){
					    	   System.out.println("Invalid Input please try again!");
					    	   ioe.printStackTrace();
					      }
					}

				});

		this.setLayout(flowLayoutButton);
		this.getContentPane().add(app, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(panel1, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(panel2, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(panel3, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		
	}
}
