package gui;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

public class Settings extends JFrame{

	/**
	 * Maintain constant UI across the frame
	 */
	private static final long serialVersionUID = 4755471415460680864L;

	public Settings() {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName()))
					;
				break;
			}
		}

		catch (Exception e) {
		}
		this.setTitle("Treatment Appointment Booking");
		this.setSize(1190,700);
		this.setPreferredSize(new Dimension(1190, 600));
		this.setResizable(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

	}

}

