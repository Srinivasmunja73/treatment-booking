package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableRowSorter;

import org.jdatepicker.impl.UtilDateModel;

import database.Doctor;
import database.DoctotTimeTableModel;

public class SearchDoctor extends Settings{
    /**
	 * 
	 */
	private static final long serialVersionUID = -5756335556597247340L;
	GUI_panel app;
    PatientMenu patientmenu;
    public GridBagLayout gridBagLayoutAppFrame;
    FlowLayout flowLayoutButton;
    GridLayout gridlayout;
	GridBagLayout gridBagLayoutMainPanel;
	public JPanel panel2;
	public JButton search;
    private JLabel searchDoctor,searchby, text;
    private JTextField textfield;
    private JComboBox<String> searchbox;
    
    UtilDateModel model = new UtilDateModel();
	JTable doctorTable;
	
	DoctotTimeTableModel tableModel;
	TableRowSorter<DoctotTimeTableModel> sorter;
	
	private final int columns = 2;
	private final int rows = 2;
	private final int vGap = 4;
	private final int hGap = 4;
	
	public SearchDoctor() {
		setupGUI();
	}

	private void setupGUI() {
		
		app = new GUI_panel();
		app.setPreferredSize(new Dimension (1190, 200));
		patientmenu = new PatientMenu();
		
        flowLayoutButton = new FlowLayout();
		flowLayoutButton.setAlignment(FlowLayout.CENTER);
		
        //call top panel for the image
		app = new GUI_panel();
		app.setPreferredSize(new Dimension (1190, 200));

		
		JPanel panel2 = new JPanel();// creates a new panel
		searchDoctor= new JLabel("<HTML><U>Search Doctor</U></HTML>");
		searchDoctor.setForeground(Color.BLUE);
		searchDoctor.setFont(new Font("Serif", Font.PLAIN, 20));
		//set panel properties and attributes
		panel2.setPreferredSize(new Dimension(1190, 30));
		panel2.add(searchDoctor);
		panel2.setLayout(flowLayoutButton);
		
		//create  panel
		//, text
		JPanel seachpanel = new JPanel();
		searchby = new JLabel("Search By :");
		text = new JLabel("Text :");
		
		//new_bookingTxt
		textfield = new JTextField();
		
		//categoryComboBox, doctorComboBox;
		 searchbox = new JComboBox<String>();
		 searchbox.addItem("name");
		 searchbox.addItem("Area of Expertise");
		

		//add components to booking panel
		 gridlayout = new GridLayout(rows, columns);
		 seachpanel.add(searchby );
		 seachpanel.add(searchbox);
		 seachpanel.add(text);
		 seachpanel.add(textfield);
	
		 seachpanel.setLayout(gridlayout);
		 gridlayout.setVgap(vGap);
		 gridlayout.setHgap(hGap);
		 
		 JPanel tablePanel = new JPanel();
		 doctorTable = new JTable();
		 try { 

				tableModel = new DoctotTimeTableModel ();
				FileInputStream patient = new FileInputStream("src/database/doctor.txt");//load file for I/)
				BufferedReader br = new BufferedReader(new InputStreamReader(patient));
				String line; 
				List<Doctor> patientList = new ArrayList<Doctor>();
				//add data to patient details table
				while((line = br.readLine()) != null)
				{
					String[] splitData = line.split(",");
					// add patient details to the table
					Doctor doc = new Doctor();
					doc.setId(splitData[0]);
					doc.setName(splitData[1]);
					doc.setAddress(splitData[2]);
					doc.setMobile(splitData[3]);
					doc.setArea_of_Specilisation(splitData[4]);

					patientList.add(doc);
				}
				tableModel.setList(patientList);
				doctorTable.setModel(tableModel);	

				br.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			sorter = new TableRowSorter<>(tableModel);
			doctorTable.setRowSorter(sorter);
		 tablePanel.add(new JScrollPane(doctorTable));
		 tablePanel.setSize(1190, 200);
		 tablePanel.setPreferredSize(new Dimension(1190,200));

		 
       
		//Panel 4 containing the button
		JPanel panel4 = new JPanel();// creates a new panel
		search = new JButton("Search");
		
		//add action listener to search button
		search.addActionListener(new ActionListener()// add action listener 
				{   
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				String text = textfield.getText();
				if (text.length() == 0) {
					sorter.setRowFilter(null);
				} else {
					sorter.setRowFilter(RowFilter.regexFilter(text));
				}
			}

				});
		
		
		// add cation listener to search box
		textfield.getDocument().addDocumentListener(new DocumentListener() {
					@Override
					public void insertUpdate(DocumentEvent e) {
						search(textfield.getText().toString());
					}
					@Override
					public void removeUpdate(DocumentEvent e) {
						search(textfield.getText().toString());
					}
					@Override
					public void changedUpdate(DocumentEvent e) {
						search(textfield.getText().toString());
					}
					public void search(String str) {
						if (str.length() == 0) {
							sorter.setRowFilter(null);
						} else {
							sorter.setRowFilter(RowFilter.regexFilter(str));
						}
					}

				});

		//set panel properties and attributes
		panel4.setPreferredSize(new Dimension(1190, 50));
		panel4.add(search);
		panel4.setLayout(flowLayoutButton);
	
		

		this.setLayout(flowLayoutButton);
		this.getContentPane().add(app, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.add(patientmenu.panel2, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(patientmenu.patientname, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.add(panel2, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		this.add(seachpanel, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		this.add(panel4, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		this.add(tablePanel, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
	}
	
}
