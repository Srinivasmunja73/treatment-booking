package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.TableRowSorter;

import database.Appointment;
import database.AppointmentsTableModel;
import database.TreatmentTableModel;
import gui.Settings;

public class PatientCancelBooking extends Settings{
	DoctorMenu doc;
	private static final long serialVersionUID = -7053327911308836914L;
	GUI_panel guipanel;
	PatientMenu patientmenu;
	public JPanel panel, panel2, panel3;
	public JTable table;
	private JButton cancel;
	JLabel treatmentH;
	FlowLayout flowLayoutButton;
	public GridBagLayout gridBagLayoutAppFrame;
	AppointmentsTableModel tableModel2;TableRowSorter<TreatmentTableModel> sorter2;
	
	private String selectedId, chosen_name, treatment;
	public java.util.Date selecteddate;

	public PatientCancelBooking() {
		setupGUI();
	}
	private void setupGUI() {
		guipanel = new GUI_panel();
		guipanel.setPreferredSize(new Dimension (1190, 200));

		flowLayoutButton = new FlowLayout();
		flowLayoutButton.setAlignment(FlowLayout.CENTER);

		panel = new JPanel();
		treatmentH= new JLabel("<HTML><U>Cancel Booking</U></HTML>");
		treatmentH.setForeground(Color.BLUE);
		treatmentH.setFont(new Font("Serif", Font.PLAIN, 20));
		//set panel properties and attributes
		panel.setPreferredSize(new Dimension(1190, 30));
		panel.add(treatmentH);
		panel.setLayout(flowLayoutButton);

		//create a panel for patients treatment history
		panel2 = new JPanel();
		table = new JTable();


        // get patient bookings
		try { 

			FileInputStream appointment = new FileInputStream("src/database/Appointments.txt");//load file for I/)
			BufferedReader bookings = new BufferedReader(new InputStreamReader(appointment));
			String line;
			
			patientmenu = new PatientMenu();
			String name = patientmenu.namelbl.getText();


			tableModel2 = new AppointmentsTableModel();
			List<Appointment> appointmetList = new ArrayList<Appointment>();
			//add data to treatment history table
			while((line = bookings.readLine()) != null)
			{
				String[] splitData = line.split(",");
				if (splitData.length>1)
					line = splitData[1];
				if(line.equalsIgnoreCase(name))
				{
					Appointment app = new Appointment ();
					app.setId(splitData[0]);
					app.setName(splitData[1]);
					app.setPhysician(splitData[2]);
					app.setTratment(splitData[3]);
					app.setDate(splitData[4]);
					app.setStatus(splitData[6]);
					app.setAppointment_time(splitData[5]);

					appointmetList.add(app);
				}
			}
			tableModel2.setList(appointmetList);
			table.setModel(tableModel2);		
			bookings.close();
			//filter data in table using jtextfield

		} catch (Exception e) {
			e.printStackTrace();
		}


		table.setPreferredSize(new Dimension (600,200));
		panel2.add(new JScrollPane(table));
		panel2.setSize(1190, 200);
		panel2.setPreferredSize(new Dimension(1190,200));
		
		// create cancel button
		panel3 = new JPanel();
		cancel = new JButton("Cancel");
		panel3.add(cancel);
		panel3.setLayout(flowLayoutButton);
		
		// add action listener to jTable
		table.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(final MouseEvent e) {
						if (e.getClickCount() >= 1) {

							selectedId = (String) table.getValueAt(table.getSelectedRow(), 0);

							//selecteddate = (Date) table.getValueAt(table.getSelectedRow(), 4);
							int row = table.getSelectedRow();
							int col = table.getSelectedColumn();
							treatment = (String) table.getValueAt(row, 3);
							chosen_name = (String) table.getValueAt(table.getSelectedRow(), 6);
						}
					}
				});
		
		cancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					File appointmnents = new File("src/database/Appointments.txt");
					File tempFile = new File("myTempFile.txt");
					
					FileInputStream bookingtt = new FileInputStream("src/database/BookingTimeTable.txt");
					BufferedReader tt = new BufferedReader(new InputStreamReader(bookingtt));

					BufferedReader reader = new BufferedReader(new FileReader(appointmnents));
					BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
					
                    String line, re_sign;
					while ((line = reader.readLine()) != null)
					{
						// trim newline when comparing with lineToRemove
						String trimmedLine = line.trim();
						if (trimmedLine.contains(selectedId))continue;
						writer.write(line + System.getProperty("line.separator"));

					}
					
					writer.close(); 
					reader.close();
					appointmnents.delete(); 
					tempFile.renameTo(appointmnents);
					new Options_Pane("Success!", "Appointment Cancelled", 1);
				    PatientCancelBooking main = new PatientCancelBooking(); 
				    main.setVisible(true);
					Component cancel_booking = (Component) e.getSource();
					Window bk = SwingUtilities.windowForComponent(cancel_booking);
					bk.setVisible(false);
				/**	String oldvalue = "";
					//re sign the cancelled row
					while (( re_sign = tt.readLine()) != null)
					{
						String[] splitData = re_sign.split("\\,");
						PatientMenu patmenu = new PatientMenu();
						
						if (splitData.length > 1)
							re_sign = splitData[0];
						   if(selecteddate == patmenu.selectedValue && chosen_name == patmenu.appointment_time)
						   {
							   oldvalue = oldvalue + re_sign + System.lineSeparator(); continue;
						   }
							String newContent = oldvalue.replace(patmenu.selectedCell, treatment);
							FileWriter wrf = new FileWriter("src/database/BookingTimeTable.txt");
							wrf.write(newContent);
							tt.close();
							wrf.close();

					}**/



				}

				catch (FileNotFoundException ex) {
					Logger.getLogger(DbLogin.class.getName()).log(Level.SEVERE, null, ex);
				} catch (IOException ex) {
					// Logger.getLogger(DbLogin.class.getName()).log(Level.SEVERE, null, ex);
				}

			}
		});

		patientmenu = new PatientMenu();



		this.setLayout(flowLayoutButton);
		this.getContentPane().add(guipanel, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(patientmenu.panel2, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(patientmenu.patientname, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(panel, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(panel2, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(panel3, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

	}

}
