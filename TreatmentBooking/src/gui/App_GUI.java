package gui;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;


public class App_GUI extends Settings {

	private static final long serialVersionUID = -8356591684351675070L;
	static App_GUI app = null;
	GUI_panel guipane;
	Buttons buttons;

	
	public GridBagLayout gridBagLayoutAppFrame;
	
	public App_GUI() {
		setupGUI();
	}

	private void setupGUI() {
		gridBagLayoutAppFrame = new GridBagLayout();
		guipane = new GUI_panel();
		guipane.setSize(1190,300);
		buttons = new Buttons();
		buttons.setBackground(Color.WHITE);
		this.setBackground(Color.WHITE);

		this.setLayout(gridBagLayoutAppFrame);
		this.getContentPane().add(guipane, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		//add buttos to the main frame
		this.add(buttons, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		
	}

}


