package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import database.Treatment;
import database.TreatmentTableModel;

public class ViewBookings extends Settings{
	GUI_panel guipanel;
	PatientMenu patientmenu;
	private JPanel panel, panel2;
	private JTable table;
	JLabel treatmentH;
	FlowLayout flowLayoutButton;
	public GridBagLayout gridBagLayoutAppFrame;
	private static final long serialVersionUID = -9070446983467507944L;

	TreatmentTableModel tableModel;

	public ViewBookings() {
		setupGUI();
	}
	private void setupGUI() {

		guipanel = new GUI_panel();
		guipanel.setPreferredSize(new Dimension (1190, 200));

		flowLayoutButton = new FlowLayout();
		flowLayoutButton.setAlignment(FlowLayout.CENTER);

		panel = new JPanel();
		treatmentH= new JLabel("<HTML><U>Treatment History</U></HTML>");
		treatmentH.setForeground(Color.BLUE);
		treatmentH.setFont(new Font("Serif", Font.PLAIN, 20));
		//set panel properties and attributes
		panel.setPreferredSize(new Dimension(1190, 30));
		panel.add(treatmentH);
		panel.setLayout(flowLayoutButton);

		//create a panel for patients treatment history
		panel2 = new JPanel();
		table = new JTable();
		try { 

			FileInputStream treatment = new FileInputStream("src/database/Appointments.txt");//load file for I/)
			BufferedReader tr = new BufferedReader(new InputStreamReader(treatment));
			String line;
			patientmenu = new PatientMenu();
			String name = patientmenu.namelbl.getText();
			tableModel = new TreatmentTableModel();
			List<Treatment> treatmentList = new ArrayList<Treatment>();
			//add data to treatment history table
			while((line = tr.readLine()) != null)
			{
				String[] splitData = line.split(",");
				if (splitData.length>1)
					line = splitData[1];
				if(line.equalsIgnoreCase(name))
				{

					Treatment treatment1 = new Treatment();
					treatment1.setPatientid(splitData[0]);
					treatment1.setName(splitData[1]);
					treatment1.setTreatment(splitData[2]);
					treatment1.setRoom(splitData[3]);
					treatment1.setDate(splitData[4]);
					treatment1.setPhysician(splitData[5]);

					treatmentList.add(treatment1);
				}
			}
			tableModel.setList(treatmentList);
			table.setModel(tableModel);		
			tr.close();

		} catch (Exception e) {
			e.printStackTrace();
		}


		table.setPreferredSize(new Dimension (700,200));
		table.getPreferredSize();
		panel2.add(new JScrollPane(table));
		panel2.setSize(1190, 200);
		panel2.setPreferredSize(new Dimension(1190,200));




		patientmenu = new PatientMenu();
		this.setLayout(flowLayoutButton);
		this.getContentPane().add(guipanel, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(patientmenu.panel2, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(patientmenu.patientname, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(panel, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.getContentPane().add(panel2, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

	}

}
