package gui;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;


public class LoginPane extends Settings{

	private static final long serialVersionUID = 7574700588521169833L;
	static LoginPane log = null;
	GUI_panel panel;
	DbLogin login;
	GridBagLayout gridBagLayoutLogPane;
	FlowLayout flowLayoutButton;

	public LoginPane() {
		setupGUI();
	}

	private void setupGUI() {

		flowLayoutButton = new FlowLayout();
		flowLayoutButton.setAlignment(FlowLayout.CENTER);
		panel = new GUI_panel();
		panel.setPreferredSize(new Dimension (1190, 300));
		gridBagLayoutLogPane = new GridBagLayout();
		login = new DbLogin();
		login.pane1.setLayout(flowLayoutButton);
		login.pane1.setPreferredSize(new Dimension(1190,80));
		login.pane.setLayout(flowLayoutButton);
		login.pane.setPreferredSize(new Dimension(1190,80));
		
		Border margin = new EmptyBorder(30, 0, 0, 10);
		login.pane1.setBorder(margin);
		login.pane.setBorder(margin);

		this.setLayout(flowLayoutButton);
		this.getContentPane().add(panel, new GridBagConstraints(0, 0, 1, 1, 1, 2, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

		this.getContentPane().add(login.pane1, new GridBagConstraints(0, 8, 1, 1, 1, 1, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		this.add(login.pane, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
			GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));


	}

}
