package database;

import java.util.Date;

public class Timetable {
		/**
	 * @return the day
	 */
	public Date getDay() {
		return day;
	}
	/**
	 * @param dateBegin the day to set
	 */
	public void setDay(Date dateBegin) {
		this.day = dateBegin;
	}
	/**
	 * @return the schedule1
	 */
	public String getSchedule1() {
		return schedule1;
	}
	/**
	 * @param schedule1 the schedule1 to set
	 */
	public void setSchedule1(String schedule1) {
		this.schedule1 = schedule1;
	}
	/**
	 * @return the schedule2
	 */
	public String getSchedule2() {
		return schedule2;
	}
	/**
	 * @param schedule2 the schedule2 to set
	 */
	public void setSchedule2(String schedule2) {
		this.schedule2 = schedule2;
	}
	/**
	 * @return the schedule3
	 */
	public String getSchedule3() {
		return schedule3;
	}
	/**
	 * @param schedule3 the schedule3 to set
	 */
	public void setSchedule3(String schedule3) {
		this.schedule3 = schedule3;
	}
	/**
	 * @return the schedule4
	 */
	public String getSchedule4() {
		return schedule4;
	}
	/**
	 * @param schedule4 the schedule4 to set
	 */
	public void setSchedule4(String schedule4) {
		this.schedule4 = schedule4;
	}
		private Date day;
	    private String schedule1;
	    private String schedule2;
	    private String schedule3;
	    private String schedule4;


}
