package database;

public class Doctor {

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}
	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	/**
	 * @return the area_of_Specilisation
	 */
	public String getArea_of_Specilisation() {
		return Area_of_Specilisation;
	}
	/**
	 * @param area_of_Specilisation the area_of_Specilisation to set
	 */
	public void setArea_of_Specilisation(String area_of_Specilisation) {
		Area_of_Specilisation = area_of_Specilisation;
	}
	private String id;
    private String name;
    private String address;
    private String mobile;
    private String Area_of_Specilisation;
}
