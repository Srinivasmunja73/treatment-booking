package database;

public class Appointment {
	/**
	 * @return the appointment_time
	 */
	public String getAppointment_time() {
		return appointment_time;
	}
	/**
	 * @param appointment_time the appointment_time to set
	 */
	public void setAppointment_time(String appointment_time) {
		this.appointment_time = appointment_time;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the tratment
	 */
	public String getTratment() {
		return tratment;
	}
	/**
	 * @param tratment the tratment to set
	 */
	public void setTratment(String tratment) {
		this.tratment = tratment;
	}
	/**
	 * @return the physician
	 */
	public String getPhysician() {
		return physician;
	}
	/**
	 * @param physician the physician to set
	 */
	public void setPhysician(String physician) {
		this.physician = physician;
	}
	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}
	private String id;
    private String name;
    private String tratment;
    private String physician;
    private String date;
	private String status;
	private String appointment_time;

}
