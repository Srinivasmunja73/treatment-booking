package database;

public class Treatment {
	
	
	private String patientid;
    private String name;
    private String treatment;
    private String room;
    private String date;
    private String physician;
	/**
	 * @return the patientid
	 */
	public String getPatientid() {
		return patientid;
	}
	/**
	 * @param patientid the patientid to set
	 */
	public void setPatientid(String patientid) {
		this.patientid = patientid;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the treatment
	 */
	public String getTreatment() {
		return treatment;
	}
	/**
	 * @param treatment the treatment to set
	 */
	public void setTreatment(String treatment) {
		this.treatment = treatment;
	}
	/**
	 * @return the room
	 */
	public String getRoom() {
		return room;
	}
	/**
	 * @param room the room to set
	 */
	public void setRoom(String room) {
		this.room = room;
	}
	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}
	/**
	 * @return the physician
	 */
	public String getPhysician() {
		return physician;
	}
	/**
	 * @param physician the physician to set
	 */
	public void setPhysician(String physician) {
		this.physician = physician;
	}

}
