package database;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;


public class BookingTimeTable extends AbstractTableModel {

	private static final long serialVersionUID = 1432116497296360752L;
	private List<Timetable> list = new ArrayList<Timetable>();
    private String[] columnNames = {"Date", "(8am-10am)","(10am-12am)", "(2pm-4pm)","(4pm-6pm)"};

    public void setList(List<Timetable> list) {
        this.list = list;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    public int getRowCount() {
        return list.size();
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
        case 0:
            return list.get(rowIndex).getDay();
        case 1:
            return list.get(rowIndex).getSchedule1();
        case 2:
            return list.get(rowIndex).getSchedule2();
        case 3:
            return list.get(rowIndex).getSchedule3();
        case 4:
            return list.get(rowIndex).getSchedule4();
        default:
            return null;
        }
    }
}