package database;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;


public class TreatmentTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1432116497296360752L;
	private List<Treatment> list = new ArrayList<Treatment>();
    private String[] columnNames = {"Patient Id", "Name","Treatment", "Room","Date","Physician"};

    public void setList(List<Treatment> list) {
        this.list = list;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    public int getRowCount() {
        return list.size();
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
        case 0:
            return list.get(rowIndex).getPatientid();
        case 1:
            return list.get(rowIndex).getName();
        case 2:
            return list.get(rowIndex).getTreatment();
        case 3:
            return list.get(rowIndex).getRoom();
        case 4:
            return list.get(rowIndex).getDate();
        case 5:
            return list.get(rowIndex).getPhysician();
        default:
            return null;
        }
    }
}