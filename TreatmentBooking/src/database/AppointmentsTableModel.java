package database;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class AppointmentsTableModel extends AbstractTableModel {

	private static final long serialVersionUID = -7156798260638923030L;
	private List<Appointment> list = new ArrayList<Appointment>();
    private String[] columnNames = {"Patient ID", "Name","Treatment For", "Physician","Date","Status","Appointment time"};

    public void setList(List<Appointment> list) {
        this.list = list;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    public int getRowCount() {
        return list.size();
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
        case 0:
            return list.get(rowIndex).getId();
        case 1:
            return list.get(rowIndex).getName();
        case 2:
            return list.get(rowIndex).getTratment();
        case 3:
            return list.get(rowIndex).getPhysician();
        case 4:
            return list.get(rowIndex).getDate();
        case 5:
        	return list.get(rowIndex).getStatus();
        case 6:
        	return list.get(rowIndex).getAppointment_time();
        default:
            return null;
        }
    }
}