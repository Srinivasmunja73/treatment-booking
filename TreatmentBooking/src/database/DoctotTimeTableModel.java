package database;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class DoctotTimeTableModel extends AbstractTableModel{

	private static final long serialVersionUID = 8272267480707337727L;
	private List<Doctor> list = new ArrayList<Doctor>();
    private String[] columnNames = {"Doctor ID", "Name","Address", "Mobile","Area of Specialisation"};

    public void setList(List<Doctor> list) {
        this.list = list;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    public int getRowCount() {
        return list.size();
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
        case 0:
            return list.get(rowIndex).getId();
        case 1:
            return list.get(rowIndex).getName();
        case 2:
            return list.get(rowIndex).getAddress();
        case 3:
            return list.get(rowIndex).getMobile();
        case 4:
            return list.get(rowIndex).getArea_of_Specilisation();
        default:
            return null;
        }
    }
}
